import scala.sys.process._

name := "Matteo_Pelosi_hw3"

version := "0.1"

scalaVersion := "2.11.8"

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case x => MergeStrategy.first
}

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.6.4"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

libraryDependencies += "com.typesafe" % "config" % "1.3.4"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "3.0.8" % "test"

libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.2.1" exclude("org.slf4j", "slf4j-log4j12") exclude("ch.qos.logback", "slf4j-log4j12")

libraryDependencies += "org.apache.spark" % "spark-sql_2.11" % "2.2.1" exclude("org.slf4j", "slf4j-log4j12") exclude("ch.qos.logback", "slf4j-log4j12")

libraryDependencies += "org.apache.spark" % "spark-mllib_2.11" % "2.2.1" exclude("org.slf4j", "slf4j-log4j12") exclude("ch.qos.logback", "slf4j-log4j12")


mainClass in (Compile, run) := Some("com.matteopelosi.PortfolioSimulation")
mainClass in assembly := Some("com.matteopelosi.PortfolioSimulation")

//assemblyJarName in assembly := "matteo_pelosi_hw3.jar"