#!/bin/bash
#HDP Sandbox IP
HDP_USER=root
HDP_IP=sandbox-hdp.hortonworks.com
HDP_PORT=2222
# Public SSH key
PUBLIC_KEY=
# Path to the Merged dataset
MERGED_PARENT="data"
MERGED_PATH="data/merged_stocks/part-00000-770af2bb-5399-4f52-889c-291907e05327-c000.csv"
MERGED_REMOTE_PATH='~/data/merged_stocks/part-00000-770af2bb-5399-4f52-889c-291907e05327-c000.csv'
MERGED_REMOTE_DIR='~/data/merged_stocks/'
MERGED_NAME="part-00000-770af2bb-5399-4f52-889c-291907e05327-c000.csv"

TICKERS_LOCAL_PATH="data/SP500_tickers.csv"
TICKERS_REMOTE_PATH="~/data/SP500_tickers.csv"
TICKERS_REMOTE_DIR="data/"

# Path to the Jar file
JAR_PATH="target/scala-2.11/Matteo_Pelosi_hw3-assembly-0.1.jar"
JAR_REMOTE_PATH='~/job.jar'
JAR_NAME="job.jar"

#HDFS
HDFS_PARENT="data"
INPUT_DIR="data/merged_stocks/"
OUTPUT_DIR="data/output/"
OUTPUT_REMOTE_PATH='~/data/output/'
#Spark
MAIN="PortfolioSimulation"

# First run: all true
# Further runs: all false but DELETE_OUTPUT_ONSTART

ASSEMBLY_ONSTART=true
DELETE_OUTPUT_ONSTART=true
MERGED_RELOAD=true
JAR_RELOAD=true

if [ "$ASSEMBLY_ONSTART" = true ]
then
  echo "Assemblying project"
  sbt clean assembly
fi

#Checks if Merged dataset is present in the HDP Sandbox, otherwise copies it
echo "Checking if MERGED is present in $HDP_USER@$HDP_IP home folder"
if ssh -p $HDP_PORT $HDP_USER@$HDP_IP stat $MERGED_REMOTE_PATH \> /dev/null 2\>\&1
            then
                    echo "$MERGED_NAME exists at remote location $MERGED_REMOTE_PATH."
            else
                    echo "$MERGED_NAME does not exist at remote location $MERGED_REMOTE_PATH."
ssh -p $HDP_PORT $HDP_USER@$HDP_IP << HERE
  echo "Making input directory (local)."
  mkdir $MERGED_PARENT
  mkdir $MERGED_REMOTE_DIR
HERE

fi
if [ "$MERGED_RELOAD" = true ] ;
            then
                    echo "Loading $MERGED_NAME."
                    scp -P $HDP_PORT $MERGED_PATH $HDP_USER@$HDP_IP:$MERGED_REMOTE_PATH
                    scp -P $HDP_PORT $TICKERS_LOCAL_PATH $HDP_USER@$HDP_IP:$TICKERS_REMOTE_PATH
fi


#Checks if JAR is present in the HDP Sandbox, otherwise copies it
echo "Checking if JAR is present in $HDP_USER@$HDP_IP home folder"
if ssh -p $HDP_PORT $HDP_USER@$HDP_IP stat $JAR_REMOTE_PATH \> /dev/null 2\>\&1
            then
                    echo "$JAR_NAME exists at remote location $JAR_REMOTE_PATH."
            else
                    echo "$JAR_NAME does not exist at remote location $JAR_REMOTE_PATH."
                    echo "Copying $JAR_NAME to remote location $JAR_REMOTE_PATH."
                    scp -P $HDP_PORT $JAR_PATH $HDP_USER@$HDP_IP:$JAR_REMOTE_PATH

fi
if [ "$JAR_RELOAD" = true ] ;
            then
                    echo "Reloading $JAR_NAME."
                    scp -P $HDP_PORT $JAR_PATH $HDP_USER@$HDP_IP:$JAR_REMOTE_PATH
fi

if [ "$MERGED_RELOAD" = true ]
            then
                    echo "Loading $MERGED_NAME to HDFS."
ssh -p $HDP_PORT $HDP_USER@$HDP_IP << HERE
  hdfs dfs -mkdir $MERGED_PARENT
  hdfs dfs -mkdir $INPUT_DIR
  hdfs dfs -put -f $MERGED_REMOTE_PATH $INPUT_DIR
  hdfs dfs -put -f $TICKERS_REMOTE_PATH $TICKERS_REMOTE_DIR
HERE

fi

if [ "$DELETE_OUTPUT_ONSTART" = true ] ;
            then
                    echo "Removing output directory on HDFS"
ssh -p $HDP_PORT $HDP_USER@$HDP_IP << HERE
  hdfs dfs -rm -r $OUTPUT_DIR
HERE

fi

echo "Running Simulation."

if ssh -p $HDP_PORT $HDP_USER@$HDP_IP stat $OUTPUT_REMOTE_PATH \> /dev/null 2\>\&1
            then
                    echo "Output directory exists at remote location $OUTPUT_REMOTE_PATH."
ssh -p $HDP_PORT $HDP_USER@$HDP_IP << HERE
  echo "Removing output directory (local)."
  rm -r $OUTPUT_REMOTE_PATH
  echo "Making output directory (local)."
  mkdir $OUTPUT_REMOTE_PATH
HERE
            else
                    echo "Output directory does not exist at remote location $OUTPUT_REMOTE_PATH."
ssh -p $HDP_PORT $HDP_USER@$HDP_IP << HERE
  echo "Making output directory (local)."
  mkdir $OUTPUT_REMOTE_PATH
HERE

fi

ssh -p $HDP_PORT $HDP_USER@$HDP_IP << HERE
  spark-submit --deploy-mode cluster $JAR_REMOTE_PATH $INPUT_DIR $OUTPUT_DIR
  echo "Getting Results."
  hdfs dfs -get $OUTPUT_DIR $OUTPUT_REMOTE_PATH
HERE

