# Monte Carlo simulation of a Stock Portfolio in Spark
### Spark implementation of parallel simulations of Stock Portfolios managed according to a Stop Loss and Gain Plateaued policy.
#### Homework 3 - Engineering Distributed Objects for Cloud Computing (Fall 2019)
#### Matteo Pelosi, (UIN 668230223)
## Introduction
This repository hosts a Portfolio simulator that, given a random set of stocks as input, manages 
the holdings according to a Stop Loss and Gain Plateaued policy. Once any of these is triggered, the Portfolio is 
rebalanced by discarding the bad performing stocks and buying new ones.  

![picture](visualization/figs/simulation_1.png)

By running multiple simulations, it is possible to estimate the Portfolio outcome in terms of overall 
value at the end of the predefined time period.

In this context, the role of Spark is:

* Preprocessing the stocks' data collected from AlphaVantage
* Running multiple simulations in parallel

#### Prerequisites
1. Install [Java 8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
2. Install [Scala](https://www.scala-lang.org/download/)
3. Install [SBT](https://www.scala-sbt.org/release/docs/Setup.html) 

## Run Locally
It is possible to run the simulator locally by:

1. Git clone the repository via the link provided by BitBucket
2. Navigate to the download target folder
3. Set `local` to `true` in the configuration
3. Run tests `sbt clean compile test`
4. Run jobs `sbt clean compile run`

Please note that in order to re-run the jobs a second time it is necessary to delete the first
run's output folder.

Alternatively, a bash script has been developed to ease multiple runs while
using the [IntelliJ IDE](https://www.jetbrains.com/idea/) or any alternative that 
allow for custom building and run configurations.

1. Git clone the repository via the link provided by BitBucket
2. Import the folder into an IntelliJ project
3. Set `local` to `true` in the configuration
4. Select "Run" and then "Edit Configurations" from the application menu
5. Either create a new "Application" configuration by clicking on the "+" sign
or select the default one
6. In the "Before Launch" section click the "+" sign to add a "Run External Tool" configuration
7. Choose a name for the configuration, add the path to the "prerun_intellij.sh" script,
leave the other options unchanged and click ok

This way, the simulations can be run multiple times from the IntelliJ IDE
and the **prerun_intellij.sh** script will take care of deleting the old output
directory.

## Hortonworks Sandbox deployment
Before heading to the cloud, the [Hortonworks Sandbox](https://www.cloudera.com/downloads/hortonworks-sandbox.html) 
provide an easy to setup virtual environment that includes YARN, Hadoop and HDFS.

1. Download the Hortonworks Sandbox [here](https://www.cloudera.com/downloads/hortonworks-sandbox.html)
2. Download an hypervisor such as [VmWare](https://www.vmware.com/products/workstation-player.html) or [VirtualBox](https://www.virtualbox.org)
3. Go through the quick configuration [guide](https://www.cloudera.com/tutorials/getting-started-with-hdp-sandbox.html) provided by Cloudera

Once the Sandbox is correctly setup, it will show the IP address that has been assigned to it.

1. Git clone the repository via the link provided by BitBucket
2. Navigate to the download target folder
3. Set `local` to `true` in the configuration
4. Assembly the project by `sbt clean assembly`
5. SSH to the Sandbox `ssh -p $HDP_PORT $HDP_USER@$HDP_IP` by either configuring SSH keys or 
providing a password.
6. Create `data` parent directory in the local filesystem with `mkdir $MERGED_PARENT`
7. Create `data/merged_stocks` directory to first load the input dataset into the Sandbox with
`mkdir $MERGED_REMOTE_DIR`
8. Prepare an output directory to save back the results
`mkdir $OUTPUT_REMOTE_PATH`
9. Load the input dataset into the Sandbox `scp -P $HDP_PORT $MERGED_PATH $HDP_USER@$HDP_IP:$MERGED_REMOTE_PATH`
10. Load the input tickers reference into the Sandbox `scp -P $HDP_PORT $TICKERS_LOCAL_PATH $HDP_USER@$HDP_IP:$TICKERS_REMOTE_PATH`
11. Load the assembled jar into the Sandbox `scp -P $HDP_PORT $JAR_PATH $HDP_USER@$HDP_IP:$JAR_REMOTE_PATH`
12. Create `data` parent directory in the HDFS file system `hdfs dfs -mkdir $MERGED_PARENT`
13. Create `data/merged_stocks` in the HDFS file system `hdfs dfs -mkdir $INPUT_DIR`
14. Load the input dataset to the HDFS file system `hdfs dfs -put -f $MERGED_REMOTE_PATH $INPUT_DIR`
15. Load the input tickers reference into the Sandbox `hdfs dfs -put -f $TICKERS_REMOTE_PATH $TICKERS_REMOTE_DIR`

It is now possible to run the simulation by:

1. SSH to the Sandbox `ssh -p $HDP_PORT $HDP_USER@$HDP_IP`
2. Run job `spark-submit --deploy-mode cluster $JAR_REMOTE_PATH $INPUT_DIR $OUTPUT_DIR`

Please note that in order to re-run the job a second time it is necessary to delete the first
run's output folder in HDFS. This can be achieved by:

1. SSH to the Sandbox `ssh -p $HDP_PORT $HDP_USER@$HDP_IP`
2. Removing old output's folder `hdfs dfs -rm -r $OUTPUT_DIR`

While running, the Sandbox prints to the terminal a link where it is possible to follow 
the job's completion and whether any failed or finished correctly.

Once all the jobs have finished, results need to be copied back to the local file system.

1. SSH to the Sandbox `ssh -p $HDP_PORT $HDP_USER@$HDP_IP`
2. Get results from HDFS `hdfs dfs -getmerge -nl <source file path> <local sandbox destination path>`

Alternatively, a bash script has been developed to automate this process.

1. Git clone the repository via the link provided by BitBucket
2. Navigate to the download target folder
3. Run the Sandbox
4. Set the configuration variables in `deploy_local.sh`
5. Run `deploy_local.sh`

## Google Cloud Deployment

Google Cloud provides a fast, easy-to-use, fully managed cloud service for running Apache Spark and Apache Hadoop clusters 
called [Google Cloud Dataproc](https://cloud.google.com/dataproc/).

First create an account on Google Cloud by clicking [here](https://console.cloud.google.com/freetrial?_ga=2.161751484.-1045492204.1573414774).

Once in the console, use the search bar to navigate to the "Dataproc" section.

1. Click on "+ Create Cluster", review the machine configurations and confirm. The cluster will be provisioned shortly after.
2. Click on the cluster, "VM Instances", "SSH" to open an in-browser terminal.
    * Create `data` parent directory with `mkdir data`
    * Create `data/merged_stocks` with `cd data` and `mkdir merged_stocks`
    * Return to main directory `cd ..` two times
3. Click on the top-right icon and select "Upload File".
    * Upload merged stocks dataset (csv) and tickers reference file (csv)
    * Move the dataset into `data/merged_stocks` with `mv <filename>.csv data/merged_stocks/`
    * Move the tickers reference file into `data/` with `mv <filename>.csv data/`
4. Setup folder stucture on HDFS.
    * Create `data` parent directory with `hdfs dfs -mkdir /user/root/data`
    * Create `data/merged_stocks` with `hdfs dfs -mkdir /user/root/data/merged_stocks`
    * Put the dataset to HDFS with `hdfs dfs -put data/merged_stocks/<filename> /user/root/data/merged_stocks`
    * Put the tickers reference to HDFS with `hdfs dfs -put data/<filename> /user/root/data/`
5. Use the search bar to navigate to the "Storage" panel.
    * Select the bucket created along with the cluster
    * Upload the JAR
    * Click on it once the download is finished to copy its URI
6. Run the jobs, go back to the "Dataproc" panel
    * Click on "+ Submit Job"
    * Job type: Spark
    * Main class: com.matteopelosi.PortfolioSimulation
    * Jar file: the path from the bucket
    * Click "Submit"
    
Once the job has finished running, it is possible to SSH back into the master and retrieve the output from HDFS with the 
`-getmerge` command as shown in the Hortonworks Sandbox deployment.

A video of this deployment can be seen [here](https://youtu.be/bGH9SdQgHpI).

[![picture](figs/video.png)](https://youtu.be/bGH9SdQgHpI)

## Data Collection
The stocks' data is fetched from [AlphaVantage](https://www.alphavantage.co) that provides a free API 
for retrieving real-time and historical financial data.

In order to use AlphaVantage's services, it is necessary to acquire an API key by completing [this](https://www.alphavantage.co/support/#api-key) 
form.

A subset of stocks has been chosen to run this simulation to limit the amount of data to be fetched.

The subset includes all stocks present in the  [Vanguard 500](https://investor.vanguard.com/mutual-funds/profile/VFINX) fund as of 
11/15/2019. 

In its free version, [AlphaVantage](https://www.alphavantage.co) limits the queries to be:

* 5 API requests per minute
* 500 API requests per day

Therefore the fetching mechanism has to comply with these terms. 

Class `util.downloadData` is designed to wait every 5 requests and check the API's response to be valid.

The data is fetched automatically onstart when `download_data_onstart` is set to `true`.

In order to download data when starting the simulation the following settings should be in place:

1. Set the API key in the configuration's `api_key` field.
2. Set the configuration's `download_data_onstart` field to `true`.
3. Place the list of tickers in a csv file with the same schema as `data/SP500_tickers.csv`.
4. Set the configuration's `tickers.path` to the ticker's csv.

Raw data from the API is downloaded in a separate file for each ticker in the `data/stocks` folder that 
can also be customized in the configuration.

## Preprocessing
Preprocessing is performed onstart when `preprocessing_data_onstart` is set to `true` in the configuration.

* All csv files present in the download destination folder are imported in a Spark DataFrame.
* A new column indicating the correct ticker is added to distinguish stocks in the merged DataFrame.
* The DataFrame is _filtered_ to keep only the data relevant to the time period considered.
* Columns are converted to the correct types (timestamp is a Date, adjusted_close is a Double and volume is a Double).
* Finally only the columns "timestamp", "adjusted_close", "volume" are _selected_.

Then, since data for each stock is only available for working days, the valid unique dates are extracted 
directly from the DataFrame and a Map `Date -> Date` is computed to return the next valid date for every 
given date.

## Simulation

The simulation takes as input a set of stocks (or the starting cash) that can be set in the configuration and a time period and outputs 
the total value of the fund for each given day.

A simulation is carried out by two components:

* `playGame(..)` : represents one day of the simulation.
* `playSession(..)` : iterates the time period to `playGame(..)` every given day.

Every "game" (one day) is structured as such:

```
playGame(..){

    updateFundValue(..) // Get today's fund value

    // Decide whether to sell and buy or hold
    if(isTimeToTakeAction(..)){
        sell(..)
        buy(..)
    }       
    
    return end_fund // Return updated fund for next day

}
```

An action is taken in case of:

* Stop Loss: A stock that looses more than an `slP` value set in the configuration is sold.
* Gain Plateaued: A stock that doesn't earn more than a `gpP` value set in the configuration is sold.

Whenever a stock is sold, its value becomes cash and immediately afterwards the `buy` function is called.

The cash is divided equally to buy a limited random number of new stocks. Once this number is computed, tickers 
are randomly selected amongst the available options for that day and the `buy` stage completes leaving no cash.

#### Parallelization

In this implementation, reads from the database are parallelized in the preprocessing stage but not inside each simulation. However, 
all simulations are run in parallel (outer parallelism). 

```
sc.parallelize(1 to <number of simulations>)
              .map(i => playSession(..))
```

Note: Spark doesnt allow to have functions with nested RDDs, therefore `playSession(..)` cannot take an RDD as an argument.

#### Another Possible Approach

Another approach could be to run multiple simulations sequentially (no outer parallelism) but read from the distributed 
dataset inside each simulation.

This has been tested by:

1. Taking `playSession(..)` out of `sc.parallelize` in the driver class (`PortfolioSimulation`), therefore running a single 
simulation.
2. Changing the type of `df` argument of the `Fund` class from `Map` to `Dataframe`.
3. Changing the query command in `getStockValue` and `getStockPrice` methods of the `Fund` class.

To get the price of a stock from a DataFrame:

```
df.filter(df("ticker") === stock._1 && df("timestamp") === day)
  .select(df("adjusted_close")).first().getDouble(0)
```

In the end, retrieving the price of a stock is the only operation performend on the dataset inside the simulations. Given 
the size of the dataset (70 MB) and the overhead of such distributed reads, this approach was discarded in favour of the 
possibility to run simulations in parallel instead.

#### Hadoop for the queries 
In the Approach 1 (final submission) no operations on a distributed dataset are performed _inside_ each simulation, therefore 
having an Hadoop MapReduce job to run the simulation would achieve no parallelism. Passing the whole dataset as a value for the 
mapper would make Hadoop initialize exactly one Mapper and one Reducer.

In the Approach 2 the only operation performed on the distributed data is a read of the price of a given stock for a given day.
A mock Hadoop MapReduce job has been produced to show how reads can be implemented.

The MapReduce job can be found [here](https://bitbucket.org/matteopelosi_/matteo_pelosi_hw3/src/master/Matteo_Pelosi_hw3_hadoop_read.zip)

It can be tried locally by:

1. Navigate to the download target folder
2. Run jobs `sbt clean compile run`

Or deployed on AWS EMR instances as shown in this [video](https://youtu.be/CCA1FW2wsRw).

[![picture](figs/thumb.png)](https://youtu.be/CCA1FW2wsRw)

## Data Visualization

Data Visualization is performed in a Jupyter Notebook using Python, Pandas, MatPlotLib and Seaborn.

In order to run data visualization, it is necessary to download and install [Anaconda](https://www.anaconda.com/distribution/) 
that provides all the tools in a single package.

The jupyter notebook is located [here](https://bitbucket.org/matteopelosi_/matteo_pelosi_hw3/src/master/visualization/CS%20441%20-%20Homework%203%20-%20Visualization.ipynb) in the repository.
