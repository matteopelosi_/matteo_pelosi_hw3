#!/bin/bash

OUTPUT_DIR="${BASH_SOURCE%/*}/data/output"

if [ -d "$OUTPUT_DIR" ]
            then
                    echo "Directory $OUTPUT_DIR exists."
                    echo "Deleting directory $OUTPUT_DIR before running mapreduce."
                    rm -rf $OUTPUT_DIR
            else
                    echo "Directory $OUTPUT_DIR does not exist."
fi