package com.matteopelosi

import java.sql.Date

import org.apache.spark.sql.DataFrame

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

class Fund(val value: Double, val cash: Double, val stocks:List[(String, Double)], val held_since:List[(String, Date)], tickers: List[String], df: collection.Map[(Date, String), (Double, Double)]) extends Serializable {

  val stocks_map: Map[String, Double] = stocks.toMap
  val held_since_map: Map[String, Date] = held_since.toMap

  // NOTE: stop loss and gain plateaued are not in the configuration file (YET) to
  // be able to switch from approach 1 to approach 2 easily.
  // So is the amount of random stocks selected.

  val slP: Double = -0.15
  val gpP: Double = 0.15

  /**
   * Sell a list of stocks to cash
   *
   * @param day day to be considered
   * @param stocks_to_sell list of stocks to be sold
   * @return updated Fund
   */
  def sell(day: Date, stocks_to_sell: List[(String, Double)]): Fund ={
    val old_stocks = stocks
    val old_stocks_value = value - cash
    val stocks_to_sell_map = stocks_to_sell.toMap

    // Calculate new stocks
    // filter: remove stocks that when sold their held amount is 0
    // map: update the values of the remaining stocks by subtracting sold amount
    val new_stocks = old_stocks.filter((os:(String, Double)) => (os._2 - stocks_to_sell_map.getOrElse(os._1, 0.0)) != 0.0)
                               .map((os:(String, Double)) => (os._1, os._2 - stocks_to_sell_map.getOrElse(os._1, 0.0)))
    val new_stocks_map = new_stocks.toMap
    val new_stocks_value = getStocksValue(day, new_stocks)

    // Calculate current cash
    val new_cash = cash + (old_stocks_value - new_stocks_value)

    // Calculate new held since
    // Remove from held since those stocks that are no longer in the new stocks
    val new_held_since_list = held_since.filter((ohs:(String, Date)) => new_stocks_map.getOrElse(ohs._1, 0.0) != 0.0)

    new Fund(value, new_cash, new_stocks, new_held_since_list, tickers, df)

  }

  /**
   * Buy a list of stocks with cash
   *
   * @param day day to be considered
   * @param stocks_to_buy stocks to be bought
   * @return updated Fund
   */
  def buy(day: Date, stocks_to_buy: List[(String, Double)]): Fund ={
    val old_stocks = stocks
    val old_stocks_map = stocks_map
    val old_stocks_value = value - cash
    val old_held_since_map = held_since_map
    val stocks_to_buy_map = stocks_to_buy.toMap

    // Calculate new stocks
    // if stock was already in the portfolio: increase its held amount by the amount bought
    // if a stock is a new addition: add it to the held stocks
    val new_stocks_buffer = new ArrayBuffer[(String, Double)]()
    old_stocks.foreach((os:(String, Double)) => {
        new_stocks_buffer.+=((os._1, os._2 + stocks_to_buy_map.getOrElse(os._1, 0.0)))
    })
    stocks_to_buy.foreach((stb:(String, Double)) => {
      if(old_stocks_map.getOrElse(stb._1, 0.0) == 0.0){
        new_stocks_buffer.+=(stb)
      }
    })
    val new_stocks = new_stocks_buffer.toList
    val new_stocks_value = getStocksValue(day, new_stocks)

    // Calculate current cash
    val new_cash = cash + (old_stocks_value - new_stocks_value)

    // Calculate new held since
    // If the stock has just been bought (new or more) then update the held since
    // Else get the old held since
    val new_held_since_buffer = new ArrayBuffer[(String, Date)]()
    new_stocks.foreach((ns:(String, Double)) => {
      if (stocks_to_buy_map.getOrElse(ns._1, null) != null) {
        new_held_since_buffer.+=((ns._1, day))
      } else {
        new_held_since_buffer.+=((ns._1, old_held_since_map(ns._1)))
      }
    })
    val new_held_since_list = new_held_since_buffer.toList

    /*
    if(new_cash < 0) {
      throw new Exception
    }
    */

    new Fund(value, new_cash, new_stocks, new_held_since_list, tickers, df)

  }




  /**
   * Update Fund Value according to current stocks' price
   *
   * @param day day to be considered
   * @return updated Fund
   */
  def updateFundValue(day: Date):Fund = {
    val stocks_amount = getStocksValue(day, stocks)

    val new_amount = stocks_amount + cash

    new Fund(new_amount, cash, stocks, held_since, tickers, df)

  }

  /**
   * Get the total value of a list of stocks
   *
   * @param day day to be considered
   * @param l_stocks list of stocks
   * @return total value of a list of stocks
   */
  def getStocksValue(day: Date, l_stocks: List[(String, Double)]): Double ={
    l_stocks.map(s => {
      getStockValue(day, s)
    }).sum
  }

  /**
   * Get the value of a stock
   *
   * @param day day to be considered
   * @param stock stock
   * @return stock price for given day
   */
  def getStockValue(day: Date, stock: (String, Double)): Double = {
    df.getOrElse((day, stock._1), null)._1 * stock._2
  }

  /**
   * Get the stock price for a given day
   *
   * @param day day to be considered
   * @param stock stock's ticker
   * @return price
   */
  def getStockPrice(day: Date, stock: String): Double = {
    df.getOrElse((day, stock), null)._1
  }

  /**
   * Get the amount of stock that is possible to buy given a cash amount
   *
   * @param cash cash amount
   * @param stock_price stock price
   * @return amount of stock that is possible to buy
   */
  def getAmountCanBuy(cash: Double, stock_price:Double): Double = {
    cash/stock_price
  }

  /**
   * Get the value change of a stock from a date to a date
   *
   * @param from from date to consider
   * @param to to date to consider
   * @param stock stock analyzed
   * @return gain or loss in percentage
   */
  def getValueChange(from: Date, to: Date, stock: (String, Double)): Double = {
    val from_value = getStockValue(from, stock)
    val to_value = getStockValue(to, stock)
    val change = to_value-from_value

    (change/from_value)*100
  }

  /**
   * Get the total fund value (has to be called after having updated fund value)
   *
   * @return total fund value
   */

  def getFundValue:Double = {
    value
  }

  /**
   * Returns a list of stocks to sell if it is time to take action
   *
   * @param day day to be considered
   * @return list of stocks to sell
   */
  def isTimeToTakeAction(day: Date):List[(String, Double)] = {
    stopLoss(day, slP) ++ gainPlateaued(day, gpP)
  }

  /**
   * Returns a list of stocks that crossed the stoploss point
   *
   * @param day day to be considered
   * @param slP stoploss point
   * @return list of stocks to sell
   */
  def stopLoss(day: Date, slP : Double):List[(String, Double)] = {
    val stocksToSell: ArrayBuffer[(String, Double)] = new ArrayBuffer[(String, Double)]()

    stocks.foreach(s => {
      if(getValueChange(held_since_map.getOrElse(s._1, null), day, s) <= slP){
        stocksToSell+=s
      }
    })
    stocksToSell.toList
  }

  /**
   * Returns a list of stocks whose gains have plateaued
   *
   * @param day day to be considered
   * @param gpP gain plateauded threshold
   * @return list of stocks to sell
   */
  def gainPlateaued(day: Date, gpP: Double):List[(String, Double)] = {
    val stocksToSell: ArrayBuffer[(String, Double)] = new ArrayBuffer[(String, Double)]()

    stocks.foreach(s => {
      if(getValueChange(held_since_map.getOrElse(s._1, null), day, s) >= gpP){
        stocksToSell+=s
      }
    })
    stocksToSell.toList
  }

  /**
   * Returns a list of stocks to buy
   *
   * @param day day to be considered
   * @return list of stocks to buy
   */
  def stocksToBuy(day: Date):List[(String, Double)] = {
    // Decide how many stocks to buy: a random number between one and three
    val start = 1
    val end   = 3
    val rnd = new scala.util.Random
    val num = start + rnd.nextInt( (end - start) + 1 )

    // Divide the cash available equally
    val cash_per_stock = 1D*cash/num

    // Select random stocks
    val max = tickers.length
    val i = getRandomBuyIt(num, max, rnd, day)

    val tickersToBuy:List[String] = i.map(index => tickers(index))

    // Calculate buying amount
    val tickersValue:List[(String, Double)] = tickersToBuy.map(ticker => (ticker, getStockPrice(day, ticker)))

    // Return stocks to buy
    val stocksToBuy:List[(String, Double)] = tickersValue.map(ticker_price => (ticker_price._1, getAmountCanBuy(cash_per_stock, ticker_price._2)))
    stocksToBuy
  }

  /**
   * Get random tickers (indices)
   *
   * @param num number of tickers needed
   * @param max amount of possible tickers
   * @param rnd random class
   * @param day day to be considered
   * @param result list of indices (accumulator)
   * @return list of indices
   */
  def getRandomBuyIt(num: Int, max: Int, rnd:Random, day:Date, result: List[Int] = List()): List[Int] ={
    if(num == 0){
      return result
    }

    val sel = randomCheck(max, rnd, day, result)

    getRandomBuyIt(num-1, max, rnd, day, result ++ List(sel))
  }

  /**
   * Makes sure not to pick a stock twice and that the ticker selected exists for that day
   *
   * @param max amount of possible tickers
   * @param rnd random class
   * @param day day to be considered
   * @param list list of indices (accumulator)
   * @return list of indices
   */
  def randomCheck(max:Int, rnd:Random, day:Date, list:List[Int]):Int = {
    val result = rnd.nextInt(max)
    try{
      val curr_price = getStockPrice(day, tickers(result))
      if(!list.contains(result)){
        return result
      }
    } catch {
      case e: NullPointerException => //println("NOT AVAILABLE TICKER ("+max.toString+") : "+ tickers(result) + " for " + day.toString )
    }

    randomCheck(max, rnd, day, list)
  }

}
