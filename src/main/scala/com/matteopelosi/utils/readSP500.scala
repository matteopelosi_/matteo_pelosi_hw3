package com.matteopelosi.utils

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.sql.SparkSession
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.mutable.ArrayBuffer
import scala.io.{BufferedSource, Source}

object readSP500  {
  // Initialize Logger
  val LOG: Logger = LoggerFactory.getLogger(getClass)
  // Loading Config
  val settings: Config = ConfigFactory.load()
  // Loading path of file containing tickers
  val tickers_path: String = settings.getString("MonteCarlo.tickers.path")
  // Initializing list of tickers
  val tickersBuffer: ArrayBuffer[String] = new ArrayBuffer[String]()

  /**
   * Read tickers from CSV file
   * @return List of tickers
   */
  def getTickers(spark: SparkSession):List[String] = {

    import spark.implicits._

    val data = spark.read.format("csv").option("header", "true").load(tickers_path)
                    .select("Ticker")

    data.map(r => r.getString(0)).collect.toList

  }

}
