package com.matteopelosi.utils

import sys.process._
import java.io.{File, FileNotFoundException, IOException}
import java.net.{URL, URLEncoder}

import com.typesafe.config.{Config, ConfigFactory}
import org.slf4j.{Logger, LoggerFactory}

import scala.io.Source

object downloadData {
  // Initialize Logger
  val LOG: Logger = LoggerFactory.getLogger(getClass)
  // Loading Config
  val settings: Config = ConfigFactory.load()

  // Loading error message info to detect error in downloading
  val error_message: String = settings.getString("MonteCarlo.av.error_message")
  val error_message_length: Int = error_message.length

  // Destination path for stocks data
  val destination: String = settings.getString("MonteCarlo.av.destination")

  // Number of retries after connection errors
  val connection_error_retry: Int = settings.getInt("MonteCarlo.av.connection_error_retry")

  /**
   * Download tickers data from AlphaVantage
   *
   * @param tickers List of tickers
   * @param alpha_token API token for AlphaVantage
   * @param query_per_minute Number of query per minutes
   * @param query_timeout Timeout between batches of queries
   */
  def downloadData(tickers:List[String], alpha_token:String, query_per_minute:Int, query_timeout:Int): Unit ={
    Thread.sleep(query_timeout)

    tickers.foreach((t: String) => {

      val t_encoded: String = URLEncoder.encode(t, "UTF-8")

      if ((tickers.indexOf(t) != 0) && ((tickers.indexOf(t) % query_per_minute) == 0)){
        LOG.info("Pausing to comply with API limitations.")
        Thread.sleep(query_timeout)
      }

      LOG.info("Downloading data for: " + t)

      val path = destination + t + ".csv"

      retry(connection_error_retry) {
        new URL("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=" + t_encoded + "&outputsize=full&apikey=" + alpha_token + "&datatype=csv") #> new File(path) !!
      }

      if(!checkResult(path, error_message, error_message_length)){
        LOG.error("Data retrieval has not been completed correctly.")
        throw new Exception
      }
    })

  }

  /**
   * Check if downloaded file is valid or an error message is returned
   *
   * @param path Path of file to be checked
   * @param error_message Expected error message
   * @param error_message_length Expected error message length
   * @return Boolean indicating whether download was successful or not
   */
  def checkResult(path:String, error_message:String, error_message_length:Int): Boolean ={
    try {
      val source = Source.fromFile(path)
      val chars = source.buffered
      val file_content = chars.take(error_message_length).mkString

      if (file_content == error_message){
        LOG.error("File " + path + " has not been downloaded correctly.")
        false
      } else {
        LOG.info("File " + path + " has been downloaded correctly.")
        true
      }
    } catch {
      case e: FileNotFoundException =>
        LOG.error("File " + path + " not found.")
        LOG.error(e.getMessage)
        false

      case e: IOException =>
        LOG.error("IOException.")
        LOG.error(e.getMessage)
        false

    }
  }

  /**
   * Helper function that retries commands n times
   *
   * @param n Number of times the commands shall be retried
   * @param fn Function to be retried
   * @tparam T Return type
   * @return
   */
  @scala.annotation.tailrec
  def retry[T](n: Int)(fn: => T): T = {
    try {
      fn
    } catch {
      case e =>
        if (n > 1) {
          LOG.error("An error occurred while downloading data. Retrying.")
          retry(n - 1)(fn)
        }
        else throw e
    }
  }

}
