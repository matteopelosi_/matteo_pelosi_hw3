package com.matteopelosi.utils

import java.text.SimpleDateFormat
import java.sql.Date

object date {
  val DATE_FORMAT = "yyyy-MM-dd"

  def getDateAsString(d: Date): String = {
    val dateFormat = new SimpleDateFormat(DATE_FORMAT)
    dateFormat.format(d)
  }

  def convertStringToDate(s: String): Date = {
    val dateFormat = new SimpleDateFormat(DATE_FORMAT)

    new java.sql.Date(dateFormat.parse(s).getTime)
  }
}
