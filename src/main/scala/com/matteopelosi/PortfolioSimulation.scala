package com.matteopelosi

import java.io.{BufferedWriter, FileWriter}
import java.sql.Date

import au.com.bytecode.opencsv.CSVWriter
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.DoubleType
import org.slf4j.{Logger, LoggerFactory}
import utils.readSP500.getTickers
import utils.downloadData.downloadData
import utils.date.convertStringToDate
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.SparkConf
import org.apache.spark.rdd.RDD

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.util.{Failure, Try}

object PortfolioSimulation {

  def main(args: Array[String]) {

    //************ START NON LOCAL SIM CONFIG ************

    // Initialize Logger
    val LOG: Logger = LoggerFactory.getLogger(getClass)
    // Loading Config
    val settings: Config = ConfigFactory.load()

    // Is local?

    val local:Boolean = settings.getBoolean("MonteCarlo.local")

    // Token, constraints and destination for data download
    val alpha_token:String = settings.getString("MonteCarlo.av.api_key")

    val query_per_minute: Int = settings.getInt("MonteCarlo.av.query_per_minute")
    val query_timeout: Int = settings.getInt("MonteCarlo.av.query_timeout")

    val download_destination: String = settings.getString("MonteCarlo.av.destination")

    val download: Boolean = settings.getBoolean("MonteCarlo.av.download_data_onstart")

    // Preprocessing settings
    val preprocess: Boolean = settings.getBoolean("MonteCarlo.av.preprocessing_data_onstart")
    val preprocess_destination: String = settings.getString("MonteCarlo.av.preprocessing_destination")

    // Simulation settings
    val num_simulations: Double = settings.getDouble("MonteCarlo.simulation.num")

    val fund_amount: Double = settings.getDouble("MonteCarlo.fund.amount")
    val fund_cash: Double = settings.getDouble("MonteCarlo.fund.cash")
    val fund_stocks_config: List[Config] = settings.getConfigList("MonteCarlo.fund.stocks").asScala.toList

    val fund_stocks: List[(String, Double)] = fund_stocks_config match {
      case null => List[(String, Double)]()
      case _ => val fund_stocks = fund_stocks_config.map((s:Config) => {
        (s.getString("stock"), s.getDouble("amount"))
      })
        fund_stocks
    }

    val fund_held_since_config: List[Config] = settings.getConfigList("MonteCarlo.fund.held_since").asScala.toList

    val fund_held_since: List[(String, Date)] = fund_held_since_config match {
      case null => List[(String, Date)]()
      case _ => val fund_held_since = fund_held_since_config.map((s:Config) => {
        (s.getString("stock"), convertStringToDate(s.getString("date")))
      })
        fund_held_since
    }

    val startDay: Date = convertStringToDate(settings.getString("MonteCarlo.simulation.startDay"))
    val endDay: Date = convertStringToDate(settings.getString("MonteCarlo.simulation.endDay"))

    // Output

    val output_path: String = settings.getString("MonteCarlo.simulation.output")

    //************ END NON LOCAL CONFIG ************

    // Create a SparkSession to initialize Spark

    val conf: SparkConf = new SparkConf().setAppName("example")

    if(local){
      conf.setMaster("local")
    }

    val spark: SparkSession = SparkSession.builder
                                          .config(conf)
                                          .appName("Monte Carlo simulation of stocks trades.")
                                          .getOrCreate()
    import spark.implicits._

    // Get the list of tickers
    val tickers:List[String] = getTickers(spark)
    // List of not supported tickers: BF.B is BF-B, GL

    // Checks the config to either download data on start or not
    if(download){
      LOG.info("Downloading S&P 500 stocks' historical data.")
      downloadData(tickers, alpha_token, query_per_minute, query_timeout)
    }


    // Preprocessing

    // Register spark function to parse file path
    spark.udf.register("get_file_name", (path: String) => path.split("/").last.split("\\.").head)

    // Checks the config to either preprocess data on start or not
    if(preprocess){
      LOG.info("Preprocessing data to output a single file containing all stocks' data and fewer dimensions.")
      preProcessing(download_destination, preprocess_destination, spark)
    }

    // Reads preprocessed data
    LOG.info("Opening stocks data.")
    val data = spark.read.format("csv").option("header", "true").load(preprocess_destination + "/*.csv")
                    .filter($"timestamp".geq(startDay) && $"timestamp".leq(endDay))
                    .withColumn("timestamp", to_date($"timestamp", "yyyy-MM-dd"))
                    .withColumn("adjusted_close", $"adjusted_close".cast(DoubleType))
                    .withColumn("volume", $"volume".cast(DoubleType))
                    .select("timestamp","ticker",  "adjusted_close", "volume")

    // Get list of available dates
    // todo: I'm losing time here!
    val dates = data.select(data("timestamp")).distinct.orderBy($"timestamp".asc)

    val dates_buffer:ArrayBuffer[Date] = new ArrayBuffer()
    dates.collect().map(row => dates_buffer += row.getDate(0))
    val dates_list = dates_buffer.toList

    // Create Next-Day Map
    val nextDate: mutable.Map[Date, Date] = mutable.Map[Date, Date]()
    for ((f,s) <- dates_list zip dates_list.drop(1)) nextDate.put(f,s)

    // Create Stocks Map
    val stockRDD = data.rdd
    val stockMapRDD:RDD[((Date, String),(Double, Double))] = stockRDD.map(row => {
      ((row.getDate(0), row.getString(1)), (row.getDouble(2), row.getDouble(3)))
    })

    val stockMap: collection.Map[(Date, String), (Double, Double)] = stockMapRDD.collect().toMap[(Date, String), (Double, Double)]

    // Run Simulation
    LOG.info("Starting Simulation.")

    // Execute simulations in parallel
    val result_RDD = spark.sparkContext.parallelize(1 to 1000).map(i => playSession(i, dates_list, nextDate, tickers, stockMap, fund_amount, fund_stocks, fund_held_since))

    val result_df = result_RDD.flatMap(identity).toDF("simulation_id", "day", "fund_value")

    result_df.write.format("com.databricks.spark.csv")
      .option("header", "true")
      .save(output_path)

    LOG.info("Simulation Completed.")

    // Stop sparkContext
    spark.sparkContext.stop()
  }

  /**
   * Generate a single file containing all the stocks' values and add a column to store the ticker
   *
   * @param source_path location of source csv files
   * @param dest_path destination of the processed output
   * @param spark spark session
   */
  private def preProcessing(source_path: String, dest_path:String, spark: SparkSession):Unit = {
    val df = spark.read.format("csv").option("header", "true").load(source_path + "*.csv")
      .withColumn("ticker", callUDF("get_file_name", input_file_name()))
      .drop("open", "high", "low", "close", "dividend_amount", "split_coefficient")
      .select("ticker", "timestamp", "adjusted_close", "volume")

    df.coalesce(1)
      .write.format("com.databricks.spark.csv")
      .option("header", "true")
      .save(dest_path)
  }

  /**
   * Play the game: the game here is a single day sell/buy or hold process
   *
   * @param session_id  id of the current simulation
   * @param day day considered
   * @param fund fund at the beginning of the day
   * @param data stocks' data
   * @return updated fund
   */
  private def playGame(session_id:Int, day: Date, fund: Fund, data: collection.Map[(Date, String), (Double, Double)]): Fund ={
    val start = fund.updateFundValue(day)
    // Decide if time to sell and buy or hold
    val stocksToSell = start.isTimeToTakeAction(day)
    if(stocksToSell.nonEmpty){
      // - Decide what to sell and buy (stop loss, or gain plateaued)
      // --- Decide what to sell
      val afterSelling = start.sell(day, stocksToSell)
      // --- Decide what to buy checking: buy for as many funds as I have, check the stock exists for that day
      val stocksToBuy = afterSelling.stocksToBuy(day)
      val afterBuying = afterSelling.buy(day, stocksToBuy)
      return afterBuying
    }
    // Else
    // - Hold
    start
  }

  /**
   * Play session: the session here is the evolution of the portfolio across a timeperiod
   *
   * @param id id of the current session
   * @param dates list of dates being the timeframe considered
   * @param nextDate a map that given a date returns the next valid date
   * @param data stocks' data
   * @return a list containing the evolution of the portfolio for every valid date
   */
  private def playSession(id:Int, dates:List[Date], nextDate: mutable.Map[Date, Date], tickers:List[String], data: collection.Map[(Date, String), (Double, Double)], fund_amount:Double = 0.0, fund_stocks:List[(String, Double)] = null, fund_held_since:List[(String,Date)] = null): List[(Int, Date, Double)] ={

    val startDay: Date = dates.head
    val actions = mutable.Map[Date, Fund]()
    val values: ArrayBuffer[(Int, Date, Double)] = new ArrayBuffer[(Int, Date, Double)]()

    initFund(startDay, actions, tickers, data, fund_amount, fund_stocks, fund_held_since)

    dates.foreach(day => {
      val newFund = playGame(id, day, actions.getOrElse(day, null), data)
      actions.put(day, newFund)
      actions.put(nextDate.getOrElse(day, null), newFund)
      values.append((id, day, newFund.getFundValue))
    })

    val result = values.toList

    // writeCsvFile(output_path+"/"+id.toString+".csv", List("simulation_id","day", "fund_value"), result.map(r => List(r._1.toString,r._2.toString, r._3.toString)))

    result

  }

  /**
   * Initialize the fund
   *
   * @param day start day of the simulation
   * @param actions array to store the initialization as first action
   * @param data stocks' data
   */
  private def initFund(day: Date, actions: mutable.Map[Date, Fund], tickers:List[String], data:collection.Map[(Date, String), (Double, Double)], fund_amount:Double, fund_stocks:List[(String, Double)], fund_held_since:List[(String,Date)]): Unit = {
    val fund = new Fund(fund_amount, 0, fund_stocks, fund_held_since, tickers, data)

    if(fund_stocks == null || fund_amount == -1 || fund_held_since == null){
      // default option
    }

    actions.put(day, fund)
  }

  /**
   * Write result to file system
   *
   * @param fileName destination path
   * @param header header of csv
   * @param rows rows of csv
   * @return Try
   */
  def writeCsvFile(
                    fileName: String,
                    header: List[String],
                    rows: List[List[String]]
                  ): Try[Unit] =
    Try(new CSVWriter(new BufferedWriter(new FileWriter(fileName)))).flatMap((csvWriter: CSVWriter) =>
      Try{
        csvWriter.writeAll(
          (header +: rows).map(_.toArray).asJava
        )
        csvWriter.close()
      } match {
        case f @ Failure(_) =>
          // Always return the original failure.  In production code we might
          // define a new exception which wraps both exceptions in the case
          // they both fail, but that is omitted here.
          Try(csvWriter.close()).recoverWith{
            case _ => f
          }
        case success =>
          success
      }
    )

  }
