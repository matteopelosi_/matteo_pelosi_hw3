import com.matteopelosi.Fund
import org.scalatest.FlatSpec
import com.matteopelosi.utils.date.convertStringToDate

import collection.Map

class FundSpec extends FlatSpec{
  "Function Fund.sell" should "perform correctly the sell operation." in {

    val value = 1119.0
    val cash = 0.0
    val stocks_list = List(("STOCK1", 3.0), ("STOCK2", 15.0), ("STOCK3", 7.0))
    val held_since = List(("STOCK1", convertStringToDate("2019-01-02")), ("STOCK2", convertStringToDate("2019-01-02")), ("STOCK3", convertStringToDate("2019-01-02")))
    val tickers = List("STOCK1", "STOCK2", "STOCK3")
    val df = Map((convertStringToDate("2019-01-02"), "STOCK1") -> (150.0, 1000.0), (convertStringToDate("2019-01-02"), "STOCK2") -> (18.0, 13000.0), (convertStringToDate("2019-01-02"), "STOCK3") -> (57.0, 700.0))

    val start_fund = new Fund(value, cash, stocks_list, held_since, tickers, df)

    val day = convertStringToDate("2019-01-02")
    val stocks_to_sell = List(("STOCK1", 2.0), ("STOCK2", 15.0), ("STOCK3", 3.0))

    val result_fund = start_fund.sell(day, stocks_to_sell)

    val result_stocks_map = result_fund.stocks.toMap
    val result_held_since_map = result_fund.held_since.toMap

    // STOCK1: 2 units sold
    assert(result_stocks_map.getOrElse("STOCK1", null) == 1.0)
    // STOCK2: fully sold
    assert(result_stocks_map.getOrElse("STOCK2", null) == null)
    // STOCK2: held since date updated
    assert(result_held_since_map.getOrElse("STOCK2", null) == null)
    // STOCK3: 3 units sold
    assert(result_stocks_map.getOrElse("STOCK3", null) == 4.0)
    // Cash is updated accordingly
    assert(result_fund.cash == 741)

  }

  "Function Fund.buy" should "perform correctly the buy operation." in {
    val value = 1119.0
    val cash = 0.0
    val stocks_list = List(("STOCK1", 3.0), ("STOCK2", 15.0), ("STOCK3", 7.0))
    val held_since = List(("STOCK1", convertStringToDate("2019-01-01")), ("STOCK2", convertStringToDate("2019-01-02")), ("STOCK3", convertStringToDate("2019-01-02")))
    val tickers = List("STOCK1", "STOCK2", "STOCK3", "STOCK4")
    val df = Map((convertStringToDate("2019-01-02"), "STOCK1") -> (150.0, 1000.0), (convertStringToDate("2019-01-02"), "STOCK2") -> (18.0, 13000.0), (convertStringToDate("2019-01-02"), "STOCK3") -> (57.0, 700.0), (convertStringToDate("2019-01-02"), "STOCK4") -> (200.0,200.0))

    val start_fund = new Fund(value, cash, stocks_list, held_since, tickers, df)

    val day = convertStringToDate("2019-01-02")
    val stocks_to_buy = List(("STOCK1", 2.0), ("STOCK4", 3.0))

    val result_fund = start_fund.buy(day, stocks_to_buy)

    val result_stocks_map = result_fund.stocks.toMap
    val result_held_since_map = result_fund.held_since.toMap

    // STOCK1: 2 units bought
    assert(result_stocks_map.getOrElse("STOCK1", null) == 5.0)
    // STOCK1: held since date updated
    assert(result_held_since_map.getOrElse("STOCK1", null) == convertStringToDate("2019-01-02"))
    // STOCK4 (new!): 3 units bought
    assert(result_stocks_map.getOrElse("STOCK4", null) == 3.0)
    // Cash is updated accordingly
    assert(result_fund.cash == -900)
  }

  "Function Fund.updateFundValue" should "recalculate the value of the fund according to today's value of the stocks." in {
    val value = 1119.0
    val cash = 0.0
    val stocks_list = List(("STOCK1", 3.0), ("STOCK2", 15.0), ("STOCK3", 7.0))
    val held_since = List(("STOCK1", convertStringToDate("2019-01-02")), ("STOCK2", convertStringToDate("2019-01-02")), ("STOCK3", convertStringToDate("2019-01-02")))
    val tickers = List("STOCK1", "STOCK2", "STOCK3")
    val df = Map((convertStringToDate("2019-01-02"), "STOCK1") -> (150.0, 1000.0), (convertStringToDate("2019-01-02"), "STOCK2") -> (18.0, 13000.0), (convertStringToDate("2019-01-02"), "STOCK3") -> (57.0, 700.0),
                 (convertStringToDate("2019-01-03"), "STOCK1") -> (100.0, 1000.0), (convertStringToDate("2019-01-03"), "STOCK2") -> (16.0, 13000.0), (convertStringToDate("2019-01-03"), "STOCK3") -> (1.0, 700.0))

    val start_fund = new Fund(value, cash, stocks_list, held_since, tickers, df)

    val day = convertStringToDate("2019-01-03")

    val result_fund = start_fund.updateFundValue(day)

    // Value of the fund is correctly updated
    assert(result_fund.value == 547)
    //Cash amount is not changed
    assert(start_fund.cash == result_fund.cash)
  }

  "Function Fund.getStocksValue" should "return the total value of the stocks in a list for a given day." in {
    val value = 1119.0
    val cash = 0.0
    val stocks_list = List(("STOCK1", 3.0), ("STOCK2", 15.0), ("STOCK3", 7.0))
    val held_since = List(("STOCK1", convertStringToDate("2019-01-02")), ("STOCK2", convertStringToDate("2019-01-02")), ("STOCK3", convertStringToDate("2019-01-02")))
    val tickers = List("STOCK1", "STOCK2", "STOCK3")
    val df = Map((convertStringToDate("2019-01-02"), "STOCK1") -> (150.0, 1000.0), (convertStringToDate("2019-01-02"), "STOCK2") -> (18.0, 13000.0), (convertStringToDate("2019-01-02"), "STOCK3") -> (57.0, 700.0),
                 (convertStringToDate("2019-01-03"), "STOCK1") -> (100.0, 1000.0), (convertStringToDate("2019-01-03"), "STOCK2") -> (16.0, 13000.0), (convertStringToDate("2019-01-03"), "STOCK3") -> (1.0, 700.0),
                 (convertStringToDate("2019-01-03"), "STOCK4") -> (72.0, 700.0))

    val start_fund = new Fund(value, cash, stocks_list, held_since, tickers, df)

    val day = convertStringToDate("2019-01-03")
    val list_of_stocks = List(("STOCK1", 2.0), ("STOCK4", 4.0))

    val result_value = start_fund.getStocksValue(day, list_of_stocks)

    // Note: STOCK4 is not in the fund but its value is queried
    assert(result_value == 488)

  }

  "Function Fund.getStockValue" should "return the value of a stock for a given day." in {
    val value = 1119.0
    val cash = 0.0
    val stocks_list = List(("STOCK1", 3.0), ("STOCK2", 15.0), ("STOCK3", 7.0))
    val held_since = List(("STOCK1", convertStringToDate("2019-01-02")), ("STOCK2", convertStringToDate("2019-01-02")), ("STOCK3", convertStringToDate("2019-01-02")))
    val tickers = List("STOCK1", "STOCK2", "STOCK3")
    val df = Map((convertStringToDate("2019-01-02"), "STOCK1") -> (150.0, 1000.0), (convertStringToDate("2019-01-02"), "STOCK2") -> (18.0, 13000.0), (convertStringToDate("2019-01-02"), "STOCK3") -> (57.0, 700.0),
      (convertStringToDate("2019-01-03"), "STOCK1") -> (100.0, 1000.0), (convertStringToDate("2019-01-03"), "STOCK2") -> (16.0, 13000.0), (convertStringToDate("2019-01-03"), "STOCK3") -> (1.0, 700.0),
      (convertStringToDate("2019-01-03"), "STOCK4") -> (72.0, 700.0))

    val start_fund = new Fund(value, cash, stocks_list, held_since, tickers, df)

    val day = convertStringToDate("2019-01-03")

    val stock = ("STOCK4", 2.0)

    val result_value = start_fund.getStockValue(day, stock)

    // Note: STOCK4 is not in the fund but its value is queried
    assert(result_value == 144)
  }

  "Function Fund.getStockPrice" should "return the price of a stock for a given day." in {
    val value = 1119.0
    val cash = 0.0
    val stocks_list = List(("STOCK1", 3.0), ("STOCK2", 15.0), ("STOCK3", 7.0))
    val held_since = List(("STOCK1", convertStringToDate("2019-01-02")), ("STOCK2", convertStringToDate("2019-01-02")), ("STOCK3", convertStringToDate("2019-01-02")))
    val tickers = List("STOCK1", "STOCK2", "STOCK3")
    val df = Map((convertStringToDate("2019-01-02"), "STOCK1") -> (150.0, 1000.0), (convertStringToDate("2019-01-02"), "STOCK2") -> (18.0, 13000.0), (convertStringToDate("2019-01-02"), "STOCK3") -> (57.0, 700.0),
      (convertStringToDate("2019-01-03"), "STOCK1") -> (100.0, 1000.0), (convertStringToDate("2019-01-03"), "STOCK2") -> (16.0, 13000.0), (convertStringToDate("2019-01-03"), "STOCK3") -> (1.0, 700.0),
      (convertStringToDate("2019-01-03"), "STOCK4") -> (72.0, 700.0))

    val start_fund = new Fund(value, cash, stocks_list, held_since, tickers, df)

    val day = convertStringToDate("2019-01-03")

    val stock = "STOCK4"

    val result_value = start_fund.getStockPrice(day, stock)

    // Note: STOCK4 is not in the fund but its price is queried
    assert(result_value == 72)
  }

  "Function Fund.getAmountCanBuy" should "return the amount of a stock that can be bought with the cash allocated." in {
    val value = 1119.0
    val cash = 0.0
    val stocks_list = List(("STOCK1", 3.0), ("STOCK2", 15.0), ("STOCK3", 7.0))
    val held_since = List(("STOCK1", convertStringToDate("2019-01-02")), ("STOCK2", convertStringToDate("2019-01-02")), ("STOCK3", convertStringToDate("2019-01-02")))
    val tickers = List("STOCK1", "STOCK2", "STOCK3")
    val df = Map((convertStringToDate("2019-01-02"), "STOCK1") -> (150.0, 1000.0), (convertStringToDate("2019-01-02"), "STOCK2") -> (18.0, 13000.0), (convertStringToDate("2019-01-02"), "STOCK3") -> (57.0, 700.0),
      (convertStringToDate("2019-01-03"), "STOCK1") -> (100.0, 1000.0), (convertStringToDate("2019-01-03"), "STOCK2") -> (16.0, 13000.0), (convertStringToDate("2019-01-03"), "STOCK3") -> (1.0, 700.0),
      (convertStringToDate("2019-01-03"), "STOCK4") -> (72.0, 700.0))

    val start_fund = new Fund(value, cash, stocks_list, held_since, tickers, df)

    val day = convertStringToDate("2019-01-03")

    val cash_allocated1 = 200.0
    val stock_price1 = 100.0

    val cash_allocated2 = 100.0
    val stock_price2 = 400.0

    val result_value1 = start_fund.getAmountCanBuy(cash_allocated1, stock_price1)
    val result_value2 = start_fund.getAmountCanBuy(cash_allocated2, stock_price2)


    assert(result_value1 == 2)
    assert(result_value2 == 0.25)
  }

  "Function Fund.getValueChange" should "return the percentage value change for a stock in a given time period." in {
    val value = 1119.0
    val cash = 0.0
    val stocks_list = List(("STOCK1", 3.0), ("STOCK2", 15.0), ("STOCK3", 7.0))
    val held_since = List(("STOCK1", convertStringToDate("2019-01-02")), ("STOCK2", convertStringToDate("2019-01-02")), ("STOCK3", convertStringToDate("2019-01-02")))
    val tickers = List("STOCK1", "STOCK2", "STOCK3")
    val df = Map((convertStringToDate("2019-01-02"), "STOCK1") -> (100.0, 1000.0), (convertStringToDate("2019-01-02"), "STOCK2") -> (10.0, 13000.0), (convertStringToDate("2019-01-02"), "STOCK3") -> (57.0, 700.0),
      (convertStringToDate("2019-01-03"), "STOCK1") -> (50.0, 1000.0), (convertStringToDate("2019-01-03"), "STOCK2") -> (100.0, 13000.0), (convertStringToDate("2019-01-03"), "STOCK3") -> (1.0, 700.0),
      (convertStringToDate("2019-01-03"), "STOCK4") -> (72.0, 700.0))

    val start_fund = new Fund(value, cash, stocks_list, held_since, tickers, df)

    val day_from = convertStringToDate("2019-01-02")
    val day_to = convertStringToDate("2019-01-03")

    val result_value1 = start_fund.getValueChange(day_from, day_to, ("STOCK1", 3.0))
    val result_value2 = start_fund.getValueChange(day_from, day_to, ("STOCK2", 15.0))


    assert(result_value1 == -50.0)
    assert(result_value2 == 900.0)
  }

}
